import React from 'react';
import {NavLink} from "react-router-dom";

const Quotes = props => {
        return(
            <div className="Quotes">
               <p>{props.id}</p>
               <p>{props.title}</p>
               <p>{props.body}</p>
               <p>{props.date}</p>
                <button onClick={props.deleted}>Delete</button>
                <NavLink to={'/quote/' + props.id + '/edit'}>Edit</NavLink>
            </div>
        )

};
export default Quotes;
