import React, {Component, Fragment} from 'react';
import './App.css';
import {NavLink, Route, Switch} from "react-router-dom";
import SubmitNewQuote from "./containers/SubmitNewQuote/SubmitNewQuote";
import QuoteFull from "./containers/QuoteFull/QuoteFull";
import QuotesLists from "./containers/QuotesLists/QuotesLists";
import EditQuote from "./containers/EditQuote/EditQuote";

class App extends Component {
  render() {
    return (
          <div>
              <Fragment>
                  <nav className="App-nav">
                    <ul>
                        <li><NavLink to="/" exact>Quotes</NavLink></li>
                        <li><NavLink to="/add" exact>Submit new quote</NavLink></li>
                    </ul>
                  </nav>
                <Switch>
                      <Route path='/' exact component={QuotesLists}/>
                      <Route path='/add' exact component={SubmitNewQuote}/>
                      <Route path='/quote/:id' exact component={QuoteFull}/>
                      <Route path='/quote/:id/edit' exact component={EditQuote}/>
                      <Route path='/quote/' exact component={QuotesLists}/>
                </Switch>
              </Fragment>
          </div>
    );
  }
}

export default App;
