import React, {Component} from 'react';
import axios from 'axios';


class EditPost extends Component {
    state = {
        quote:  {
            author: '',
            text: ''
        },
        quotes: {
            author: '',
            category: '',
            text: ''
        },
        menu: [
            {title: 'Star Wars', id: 'star wars'},
            {title: 'Famous people', id: 'famous people'},
            {title: 'Saying', id: 'saying'},
            {title: 'Humor', id: 'humor'},
            {title: 'Motivational', id: 'motivational'}
        ],
        loading: false
    };

    componentDidMount(){
        const id = this.props.match.params.id;

        axios.get(`/quote/${id}.json`).then((response) =>{
            console.log(response.data);
            this.setState({loadedPost: response.data, quote: response.data})

        })
    }
    savePostHandler = e => {
        e.preventDefault();

        this.setState({loading: true});
        const id = this.props.match.params.id;

        axios.patch(`/quote/${id}.json`, this.state.quote).then(response => {
            this.setState({loading: false});
            this.props.history.replace('/');
        });
    };


    quotesValueChanged = e => {
        e.persist();
        this.setState(prevState => {
            return {
                quote: {...prevState.quote, [e.target.name]: e.target.value}
            }
        });
    };

    render(){
        return(
            <div>
                <select name="category" value={this.state.quotes.category} onChange={this.quotesValueChanged}>
                    {this.state.menu.map(category =>
                        <option value={category.id}>
                            {category.title}
                        </option>
                    )}
                </select>
            <form className="AddQuote">
                <h1>Edit  quote</h1>
                <input type="text" name="Author" placeholder="Author" value={this.state.quote.author} onChange={this.quotesValueChanged}/>
                <textarea name="Quote Text" placeholder="Quote Text" value={this.state.quote.text} onChange={this.quotesValueChanged}>
                </textarea>

                <button onClick={this.savePostHandler}>Save</button>
            </form>
            </div>
        )
    }
}
export default EditPost;