import React, {Component} from 'react';
import './SubmitNewQuote.css';
import axios from 'axios';
import Quotes from "../../components/Quotes/Quotes";

class  SubmitNewQuote extends Component {
    state = {
        quote:  {
            title: '',
            body: '',
            date: ''
        },
        quotes: {
            author: '',
            category: '',
            text: ''
        },
        menu: [
            {title: 'Star Wars', id: 'star wars'},
            {title: 'Famous people', id: 'famous people'},
            {title: 'Saying', id: 'saying'},
            {title: 'Humor', id: 'humor'},
            {title: 'Motivational', id: 'motivational'}
        ],
        loading: false
    };
    saveQuoteHandler = e => {
        e.preventDefault();

        this.setState({loading: true});

        axios.post('/quote.json', this.state.quote).then(response => {
            this.setState({loading: false});
            this.props.history.replace('/');
        });
    };

    quoteValueChanged = e => {
        e.persist();
        this.setState(prevState => {
            return {
                quote: {...prevState.quote, [e.target.name]: e.target.value, date: Date()}
            }
        });
    };
    quotesValueChanged = e => {
        e.persist();
        this.setState(prevState => {
            return {
                quotes: {...prevState.quotes, [e.target.name]: e.target.value}
            }
        });
    };

    render(){
        return(
            <div>
            <form className="SubmitQuote">
                <h1>Submit New Quote</h1>
                <input type="text" name="title" placeholder="Author" value={this.state.quote.title} onChange={this.quoteValueChanged}/>
                <select name="category" value={this.state.quotes.category} onChange={this.quotesValueChanged}>
                    {this.state.menu.map(category =>
                        <option value={category.id}>
                            {category.title}
                        </option>
                    )}
                </select>
                <textarea name="body" placeholder="Quote Text" value={this.state.quote.body} onChange={this.quoteValueChanged}>
                </textarea>
                <button onClick={this.saveQuoteHandler}>Save</button>

            </form>
        </div>
        )
    }

};
export default SubmitNewQuote;