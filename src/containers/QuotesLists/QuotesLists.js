import React, {Component, Fragment} from 'react';
import Quotes from "../../components/Quotes/Quotes";
import axios from 'axios'
import NavLink from "react-router-dom/es/NavLink";


class QuotesLists extends Component{
    state = {
        quote: [],


        quotes: {
            author: '',
            category: '',
            text: ''
        },
        categories: [
            {title: 'Star Wars', id: 'star wars'},
            {title: 'Famous people', id: 'famous people'},
            {title: 'Saying', id: 'saying'},
            {title: 'Humor', id: 'humor'},
            {title: 'Motivational', id: 'motivational'}
        ],
        selectedCategoryId: null

};
    componentDidMount() {
        axios.get('/quote.json').then((response) => {
            console.log(response);
            const quote = [];

            for(let key in response.data){
                quote.push({...response.data[key], id: key})
            }

            this.setState({quote})
        })
    }

    quoteValueChanged = e => {
        e.persist();
        this.setState(prevState => {
            return {
                quotes: {...prevState.quotes, [e.target.name]: e.target.value}
            }
        });
    };
    deleteQuoteHandler = id => {
        axios.delete(`/quote/${id}.json`).then(() => {
            this.setState(prevState => {
                const quote = [...prevState.quote];
                const index = quote.findIndex(quote => quote.id === id);
                quote.splice(index, 1);
                return {quote};
            })
        })
    };

    render(){
        return(
            <Fragment>
            <div>
            <select name="category" value={this.state.quotes.category} onChange={this.quoteValueChanged}>
                {this.state.categories.map(category =>
                    <option value={category.id}>
                        {category.title}
                    </option>
                )}
            </select>
                <section className='Categories'>
                    {this.state.categories.map(category =>
                        <li>
                            <NavLink
                                to={'/' + category.id}
                                key={category.id}>
                                {category.title}
                            </NavLink>
                        </li>
                    )}
                </section>
                <div className="Quotes">
                {this.state.quote.map(quote =>(
                        <Quotes
                            key={quote.id}
                            title={quote.title}
                            body={quote.body}
                            date={quote.date}
                            deleted={() => this.deleteQuoteHandler(quote.id)}
                />
            ))}
                </div>

            </div>
            </Fragment>
        )
}
}

export default QuotesLists;