import React, {Component} from 'react';
import axios from 'axios';
import {NavLink} from "react-router-dom";


class QuoteFull extends Component{
    state = {
        quote: [],
        loadedQuote: null
    };

    componentDidMount(){
        const id = this.props.match.params.id;

        axios.get(`/quote/${id}.json`).then((response) =>{
            console.log(response.data);
            this.setState({loadedQuote: response.data})
        })
    }

    deleteQuoteHandler = (e, id) => {
        e.preventDefault();
        console.log(id, '------------');
        axios.delete(`/quote/${id}.json`).then(() => {
            this.props.history.push('/');
        })
    };
    render(){
        if(this.state.loadedQuote) {
            return (
                <div className="QuoteFull">
                    <h1>{this.state.loadedQuote.title}</h1>
                    <p>{this.state.loadedQuote.body}</p>
                    <a href="#" onClick={(e) => this.deleteQuoteHandler(e, this.props.match.params.id)}>Delete</a>
                    <NavLink to={'/quote/' + this.props.match.params.id + '/edit'}>Edit</NavLink>
                </div>
            )
        } else {
            return <p>Loading...</p>
        }
    }

}
export default QuoteFull;